unit module AoC::Day20;


# Day 20: Pulse Propagation
# -------------------------

role Module {
	has Str $.name;
	has Str @.targets;

	method pulse($signal, $source) { * }

	method link(Str $target) {
		@.targets.push($target) unless $target ∈ @.targets;
	}

	method encode() { '-' }
}

class Broadcaster does Module {
	method pulse($signal, $) {
		($signal) X @!targets;
	}
}

class FlipFlop does Module {
	has $.state;

	method pulse($signal, $) {
		# High pulse is ignored
		return () if $signal;

		# Low pulse flips the state, the state then maps to signal
		$!state = !$!state;
		return ($!state) X @!targets;
	}

	method encode() { $.state ?? 'on' !! 'off' }
}

class Conjunction does Module {
	has %.sources;

	method pulse($signal, $source) {
		%!sources{$source} = $signal;
		return (!%!sources.values.all) X @!targets;
	}

	method register($input) {
		%!sources{$input} = False;
	}

	method encode() {
		%.sources.pairs.sort({ $^a.key cmp $^b.key }).map({ $^a.value ?? 'on' !! 'off' }).join(',')
	}
}

class Button does Module {
	method pulse($, $) {
		return ((False, 'broadcaster'),);
	}
}

class Watcher does Module {
	has Module $.watch handles <target>;
	has %.notify = False => 0, True => 0;

	method pulse($signal, $source) {
		%!notify{$signal}++;
		return (defined $.watch) ?? $.watch.pulse($signal, $source) !! ();
	}

	method reset() {
		%!notify{$_} = 0 for True, False;
	}
}

grammar Modules::Grammar {
	rule TOP { <moddef> '->' <target=.name>+ % ', ' }

	proto rule moddef { * }
	rule moddef:sym<broad> { 'broadcaster' }
	rule moddef:sym<ff> { '%' <name> }
	rule moddef:sym<conj> { '&' <name> }

	token name { \w+ }
}

class Modules::Actions {
	method TOP($/) {
		my $module = $/<moddef>.made;
		$module.link($_) for $/<target>.map(~*);
		make $module;
	}

	method moddef:sym<broad>($/) {
		make Broadcaster.new(name => 'broadcaster')
	}

	method moddef:sym<ff>($/) {
		make FlipFlop.new(name => ~$/<name>)
	}

	method moddef:sym<conj>($/) {
		make Conjunction.new(name => ~$/<name>)
	}
}

sub read-modules(IO::Handle $in) {
	my %m = Hash.new: gather for $in.lines.map({ Modules::Grammar.parse($^a, actions => Modules::Actions).made }) -> $m {
		take $m.name => $m
	};

	%m<button> = Button.new: name => 'button', targets => ('broadcaster');

	# Register input modules for Conjunctions
	my %known;

	for %m.values -> $module {
		for $module.targets -> $target-name {
			if %known{$target-name}:!exists {
				my $md = %m{$target-name};
				%known{$target-name} = (defined $md) && $md.isa(Conjunction)
					?? $md !! Nil;
			}

			my $target = %known{$target-name};
			$target.register($module.name) if $target !=== Any;
		}
	}

	return %m;
}

sub run(%modules, $start = 'button', $source = '', $signal = False) {
	# Starting with a button push from myself…
	my @queue = ($signal, $start, $source),;
	# … which itself does not count as a pulse.
	my %count = True => 0, False => 0;

	while @queue.elems && my (@e) = @queue.shift {
		my ($signal, $target, $source) = @e;
		%count{$signal}++ if defined $signal;

		my $module = %modules{$target};

		# There can be untyped modules spawned on demand
		next unless defined $module;

		for $module.pulse($signal, $source) -> ($s2, $t2) {
			@queue.push(($s2, $t2, $target),);
		}

	}

	%count
}

sub encode-state(%modules) {
	%modules.pairs.sort({ $^a.key cmp $^b.key }).map(*.value.encode).join(';')
}

sub consolidate(@cache, $i, $j, $count) {
	my @count;

	for @cache[0 .. $i - 1] -> $s {
		@count[$_] += $s[$_ + 1] for 0 .. 1;
	}

	my $n = 0;
	if $j > $i {
		$n = 1000 div ($j - $i);

		for @cache[$i ..^ $j] -> $s {
			@count[$_] += $n * $s[$_ + 1] for 0 .. 1;
		}
	}

	for @cache[$i ..^ ($count - $n * ($j - $i) - $i - 1)] -> $s {
		@count[$_] += $s[$_ + 1] for 0 .. 1;
	}

	[*] @count
}

sub run-many(%modules, $count) {
	my @cache;

	for 0 ..^ $count -> $i {
		my %next = run(%modules);
		my $state = encode-state(%modules);

		for @cache.kv -> $j, $cached {
			if $cached[0] eq $state {
				return consolidate(@cache, $j, $i, $count);
			}
		}

		@cache[$i] = ($state, %next<True>, %next<False>);
	}

	return consolidate(@cache, $count, $count, $count);
}

our sub part1(IO::Handle $in) {
	my %modules = read-modules($in);
	run-many(%modules, 1000)
}

sub find-output(%modules, Str $input, %seen) {
	return Nil if %seen{$input};
	%seen{$input} = True;

	my $module = %modules{$input};
	if $module.isa(Conjunction) && $module.sources.elems == 1
			&& $module.targets.elems == 1 {
		return $input;
	}

	for $module.targets -> $target {
		if defined(my $output = find-output(%modules, $target, %seen)) {
			return $output;
		}
	}

	return Nil;
}

our sub part2(IO::Handle $in) {
	my %modules = read-modules($in);

	# If the input is displayed as DOT graph format, we can see that
	# machines are split into components with input and output nodes:
	#
	# □ Input for these components is always a low signal from ‹broadcaster›,
	# □ Output is a single conjunction node that acts like a signal inversion.
	#
	# Output nodes are than sinked into a single conjunction, which passes
	# its signal to ‹rx›. Getting a single low signal to ‹rx› requires
	# high signals delivered to the sink; this means, output nodes must
	# get a single low signal (as they act as inverters).
	#
	# Getting the total number of button presses can then be determined as
	# the least common multiply for button presses required to get low
	# signals from each component. Proof left as an exercise to the reader.

	[lcm] %modules<broadcaster>.targets.race.map(-> $input {
		my $output = find-output(%modules, $input, Hash.new());

		die "Cannot find an output node for $input"
			if !defined $output;

		%modules{$output} = Watcher.new: watch => %modules{$output};

		my $i = 0;

		repeat {
			%modules{$output}.reset;
			run(%modules, $input, 'broadcaster', False);
			$i++;
		} until %modules{$output}.notify{False} == 1;

		$i
	});
}
